const axios = require('axios');
const { expect } = require('chai');

const sampleAppId = `b6907d289e10d714a6e88b30761fae22`

describe('API Tests', () => {
    test.each([[35, 139, 'Tawarano', 'JP'], [350, 139, 'Tawarano', 'JP']])
        ('Current weather for lat: %s and lon: %s', (lat, lon, expectName, expectCountry) => 
            axios.get(`https://samples.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${sampleAppId}`)
            .then(function (response) {
                expect(response.status).equals(200);
                expect(response.data.name).equals(expectName);
                expect(response.data.sys.country).equals(expectCountry);
            })
    );

    test.each([[94040, 'Mountain View', 'US'], [91001, 'Mountain View', 'US']])
        ('Current weather for zip: %s', (zip, expectName, expectCountry) => 
            axios.get(`https://samples.openweathermap.org/data/2.5/weather?zip=${zip}&appid=${sampleAppId}`)
            .then(function (response) {
                expect(response.status).equals(200);
                expect(response.data.name).equals(expectName);
                expect(response.data.sys.country).equals(expectCountry);
            })
    );
});