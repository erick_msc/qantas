const { Selector } = require('testcafe');

class Link {
    constructor (text) {
        this.link = Selector(`.hc-flightsnav a`).withText(text);
    }
}

class Nav {
    constructor () {
        this.hotels = new Link('Hotels');
        this.flights = new Link('Flights');
        this.cars = new Link('Cars');
    }
}

module.exports = Nav;