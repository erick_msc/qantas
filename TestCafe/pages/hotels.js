const { Selector } = require('testcafe');

class Link {
    constructor (text) {
        this.link = Selector(`.hc-flightsnav a`).withText(text);
    }
}

class Hotels {
    constructor () {
        this.form = Selector(`#hc_searchBox form`);
        this.destination = {
            input: this.form.find('#hc_f_id_where_1'),
            dropdown: text => Selector('ul > li > a').withText(text)
        };
        this.checkIn = {
            day: {
                select: this.form.find('.hc_f_checkin .hc_f_day select'),
                option: text => this.form.find('.hc_f_checkin .hc_f_day select option').withText(text)
            },
            month: {
                select: this.form.find('.hc_f_checkin .hc_f_month select'),
                option: text => this.form.find('.hc_f_checkin .hc_f_month select option').withText(text)
            }
        };
        this.checkOut = {
            day: {
                select: this.form.find('.hc_f_checkout .hc_f_day select'),
                option: text => this.form.find('.hc_f_checkout .hc_f_day select option').withText(text)
            },
            month: {
                select: this.form.find('.hc_f_checkout .hc_f_month select'),
                option: text => this.form.find('.hc_f_checkout .hc_f_month select option').withText(text)
            }
        };
        this.searchButton = this.form.find('a').withText('Search');
        this.resultTitle = Selector('.hc-searchproperties');
    }
}

module.exports = Hotels;