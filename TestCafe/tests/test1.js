const Nav = require('../pages/nav');
const Hotels = require('../pages/hotels');

// Replaced original site with https://www.hotelscombined.com.au/ as unable to access original site; no credentials provided
fixture `Home Page`
    .page `https://www.hotelscombined.com.au/`;
const nav = new Nav();
const hotels = new Hotels();

test('Search Hotel for a weekend (Sydney)', async t => {
    await t
        .click(nav.hotels.link)

        // Check in
        .click(hotels.checkIn.day.select)
        .click(hotels.checkIn.day.option('4')) // Bug: App inconsistency
        .click(hotels.checkIn.month.select)
        .click(hotels.checkIn.month.option('October 2019'))

        // Check out
        .click(hotels.checkOut.day.select)
        .click(hotels.checkOut.day.option('Sun 6')) // Bug: App inconsistency
        .click(hotels.checkOut.month.select)
        .click(hotels.checkOut.month.option('October 2019'))
        
        // Place
        .typeText(hotels.destination.input, 'Sydney')
        .click(hotels.destination.dropdown('Sydney, Australia'))

        // Search
        .click(hotels.searchButton)
        // .expect(hotels.resultTitle.innerText).contains('Sydney');
});

test('Search Hotel for next December (Melbourne)', async t => {
    await t
        .click(nav.hotels.link)

        // Check in
        .click(hotels.checkIn.day.select)
        .click(hotels.checkIn.day.option('23')) // Bug: App inconsistency
        .click(hotels.checkIn.month.select)
        .click(hotels.checkIn.month.option('December 2019'))

        // Check out
        .click(hotels.checkOut.day.select)
        .click(hotels.checkOut.day.option('Sun 29')) // Bug: App inconsistency
        .click(hotels.checkOut.month.select)
        .click(hotels.checkOut.month.option('December 2019'))
        
        // Place
        .typeText(hotels.destination.input, 'Melbourne')
        .click(hotels.destination.dropdown('Melbourne, Australia'))

        // Search
        .click(hotels.searchButton)
        // .expect(hotels.resultTitle.innerText).contains('Melbourne');
});


test.skip('Search Weekend Flights to Bali', async t => {

});

test.skip('Search December one way flight to Melbourne', async t => {

});

test.skip('Search Car for next weekend (Sydney)', async t => {

});

test.skip('Book Car for next December (Melbourne)', async t => {

});


